Server Concurrency Exercises
----------------------------

# 1. In-memory Request Caching
Use a stateful goroutine to serialize access to a request to response map and check the store before executing a request. This example lacks any cache store management (i.e disgarding outdated responses, limiting cache size, etc...).

```
go run cache.go
```

# 2. Request Collapsing
Using again a stateful goroutine, request collapsing is achieved by checking for ongoing requests before execution and if one is found, adding a listener for the ongoing request's broadcast of the result once it has returned.

```
go run request_collapsing.go
```

# 3. Request Replication
With the help of Google's [context](https://godoc.org/golang.org/x/net/context) package, create a context for each incoming request, creating several RPC requests on its behalf that all share the same context, retrieve the fastest response and subsequently cancel the remaining requests that were too slow.

```
go run replication.go
```
