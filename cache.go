/*
  In-memory Cache and 3 Concurrent Request Handlers
*/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

type cache struct {
	readChan  chan *readOp
	writeChan chan *writeOp
}

type readOp struct {
	key  int
	resp chan string
}

type writeOp struct {
	key   int
	value string
}

func NewCache() *cache {
	c := new(cache)
	c.readChan = make(chan *readOp)
	c.writeChan = make(chan *writeOp)
	go c.init()
	return c
}

func (c *cache) init() {
	var cache = make(map[int]string)
	for {
		select {
		case read := <-c.readChan:
			read.resp <- cache[read.key]
		case write := <-c.writeChan:
			cache[write.key] = write.value
		}
	}
}

func (c *cache) read(n int) string {
	read := &readOp{
		key:  n,
		resp: make(chan string),
	}
	c.readChan <- read
	return <-read.resp
}

func (c *cache) write(key int, value string) {
	write := &writeOp{
		key:   key,
		value: value,
	}
	c.writeChan <- write
}

func main() {
	c := NewCache()
	requests := requestGen(5)
	results := make(chan string)
	for i := 0; i < 3; i++ {
		go requestHandler(requests, c, results)
	}

	for i := 0; i < 5; i++ {
		fmt.Println("Got result: ", <-results)
	}
}

func requestGen(number int) chan int {
	out := make(chan int, number)
	go func() {
		// without ticker here, result would not return before subsequent calls. Would need request collapsing for those cases.
		ticker := time.Tick(100 * time.Millisecond)
		for i := 0; i < number; i++ {
			<-ticker
			out <- 259
		}
		close(out)
	}()
	return out
}

func requestHandler(requests chan int, c *cache, results chan string) {

	for r := range requests {
		start := time.Now()
		// Check for joke in cache
		joke := c.read(r)
		if joke != "" {
			results <- joke
			fmt.Println("Time spent on request: ", time.Since(start))
			continue
		}

		fmt.Println("RPC call used.")
		joke, err := getJoke(r)
		if err != nil {
			panic(err)
		}

		// store joke in cache
		// Todo: limit size of cache & lifetime of resident jokes
		c.write(r, joke)

		results <- joke
		fmt.Println("Time spent on request: ", time.Since(start))
	}

}

func getJoke(id int) (string, error) {

	// For testing, set a more consistent amount of randomness
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
	return "Insert Chuck Norris Joke Here", nil

	// url := "http://api.icndb.com/jokes/" + strconv.Itoa(id)
	// req, err := http.NewRequest("GET", url, nil)
	// if err != nil {
	//  return "", err
	// }

	// resp, err := http.DefaultClient.Do(req)
	// if err != nil {
	//  return "", err
	// }

	// defer resp.Body.Close()
	// buf := new(bytes.Buffer)
	// buf.ReadFrom(resp.Body)
	// bytes := buf.Bytes()

	// joke := struct {
	//  Type  string `json:"type"`
	//  Value struct {
	//    Id   int    `json:"id"`
	//    Joke string `json:"joke"`
	//  } `json:"value"`
	// }{}
	// err = json.Unmarshal(bytes, &joke)
	// if err != nil {
	//  return "", err
	// }

	// return joke.Value.Joke, nil

}
