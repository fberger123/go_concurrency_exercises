/*
  Request Replication Implementation
*/

package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/net/context"
)

type jokeResp struct {
	err  error
	joke string
}

func main() {
	errChan := make(chan error)
	results := make(chan string)
	requests := genRequests(259)

	go func() {
		for r := range requests {
			fmt.Println("Sending request: ", r)
			go requestHandler(r, results, errChan)
		}
	}()

	for {
		select {
		case joke := <-results:
			fmt.Println("Got: ", joke)
		case err := <-errChan:
			fmt.Println("Error: ", err)
		}
	}
}

func requestHandler(id int, results chan string, errChan chan error) {
	start := time.Now()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	jokes := make(chan *jokeResp)

	for i := 0; i < 3; i++ {
		go fetchJoke(ctx, id, jokes)
	}

	select {
	case response := <-jokes:
		if response.err != nil {
			errChan <- response.err
		} else {
			results <- response.joke
		}
	}
	fmt.Println("Request took: ", time.Since(start))
}

func fetchJoke(ctx context.Context, id int, jokes chan *jokeResp) {
	jokeResponse := &jokeResp{}
	url := "http://api.icndb.com/jokes/" + strconv.Itoa(id)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		jokeResponse.err = err
		jokes <- jokeResponse
		return
	}

	var joke string

	err = httpDo(ctx, req, func(resp *http.Response, err error) error {
		if err != nil {
			return err
		}
		jokeBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		joke = string(jokeBytes)
		return nil
	})
	if err != nil {
		jokeResponse.err = err
		jokes <- jokeResponse
		return
	}
	jokeResponse.joke = joke
	jokes <- jokeResponse
}

func httpDo(ctx context.Context, req *http.Request, f func(*http.Response, error) error) error {
	tr := &http.Transport{}
	client := &http.Client{Transport: tr}
	c := make(chan error, 1)
	go func() {
		c <- f(client.Do(req))
		// c <- f(fakeClientReq()) //Fake request for testing
	}()
	select {
	case <-ctx.Done():
		tr.CancelRequest(req)
		fmt.Println("Request too slow. Canceled")
		<-c
		return ctx.Err()
	case err := <-c:
		return err
	}
}

/*
  Construct Fake Requests for testing
*/
type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }

func fakeClientReq() (*http.Response, error) {
	timer := time.NewTimer(time.Duration(rand.Intn(1000)) * time.Millisecond)
	<-timer.C
	body := nopCloser{bytes.NewBufferString("A Joke Here!")}
	res := &http.Response{
		Status: "200 OK",
		Body:   io.ReadCloser(body),
	}
	return res, nil
}

func genRequests(requestId int) chan int {
	out := make(chan int)
	go func() {
		ticker := time.Tick(time.Second)
		for {
			<-ticker
			out <- requestId
		}
	}()
	return out
}
