/*
  Request Collapsing and 3 Concurrent Request Handlers
*/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

type collapser struct {
	readChan   chan *lookupOp
	addChan    chan int
	removeChan chan int
	listenChan chan *listenOp
	done       chan bool
}

type lookupOp struct {
	key  int
	resp chan *lookup
}

type lookup struct {
	ok        bool
	listeners []chan string
}

type listenOp struct {
	key      int
	listener chan string
}

func NewCollapser() *collapser {
	c := new(collapser)
	c.readChan = make(chan *lookupOp)
	c.addChan = make(chan int)
	c.removeChan = make(chan int)
	c.listenChan = make(chan *listenOp)
	c.done = make(chan bool)
	go c.init()
	return c
}

func (c collapser) init() {
	var ongoingRequests = make(map[int][]chan string)
	for {
		select {
		case read := <-c.readChan:
			// fmt.Println(ongoingRequests)
			listeners, ok := ongoingRequests[read.key]
			lookupResp := &lookup{
				ok:        ok,
				listeners: listeners,
			}
			read.resp <- lookupResp
		case add := <-c.addChan:
			ongoingRequests[add] = []chan string{}
		case remove := <-c.removeChan:
			delete(ongoingRequests, remove)
		case listen := <-c.listenChan:
			ongoingRequests[listen.key] = append(ongoingRequests[listen.key], listen.listener)
		case <-c.done:
			close(c.readChan)
			close(c.addChan)
			close(c.removeChan)
			close(c.listenChan)
			return
		}
	}
}

func (c collapser) listen(id int) chan string {
	listener := make(chan string)
	listen := &listenOp{
		key:      id,
		listener: listener,
	}
	c.listenChan <- listen
	return listener
}

func (c collapser) read(n int) *lookup {
	read := &lookupOp{
		key:  n,
		resp: make(chan *lookup),
	}
	c.readChan <- read
	return <-read.resp
}

func (c collapser) add(key int) {
	c.addChan <- key
}

func (c collapser) remove(key int) {
	c.removeChan <- key
}

func (c collapser) release() {
	c.done <- true
}

func (c collapser) broadcast(id int, value string) {
	fmt.Println("Broadcast called")
	lookup := c.read(id)
	if !lookup.ok {
		panic("Called broadcast on request that is not currently ongoing")
	}
	for _, l := range lookup.listeners {
		l <- value
	}
}

func main() {
	c := NewCollapser()
	requests := requestGen(5)
	results := make(chan string)
	start := time.Now()
	for i := 0; i < 3; i++ {
		go requestHandler(requests, results, c)
	}

	for i := 0; i < 5; i++ {
		res := <-results
		fmt.Println("Got result: ", res)
	}
	close(results)
	c.release()
	fmt.Println(time.Since(start))
}

func requestGen(number int) chan int {
	out := make(chan int, number)
	go func() {
		ticker := time.Tick(1 * time.Millisecond)
		for i := 0; i < number; i++ {
			<-ticker
			out <- 259
		}
		close(out)
	}()
	return out
}

func requestHandler(requests chan int, results chan string, c *collapser) {

	for r := range requests {

		// Check if this exact request is already ongoing
		lookup := c.read(r)
		if lookup.ok {
			// make this request listen to result of ongoing request
			listener := c.listen(r)
			go awaitResponse(results, listener)
			continue
		} else {
			c.add(r) // Add request to ongoing
		}

		fmt.Println("RPC call used.")
		joke, err := getJoke(r)
		if err != nil {
			panic(err)
		}

		c.broadcast(r, joke)
		c.remove(r)

		results <- joke
	}

}

func awaitResponse(results chan string, listener chan string) {
	results <- <-listener
	close(listener)
}

func getJoke(id int) (string, error) {

	// For testing, set a more consistent amount of randomness
	time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
	return "Insert Chuck Norris Joke Here", nil

	// url := "http://api.icndb.com/jokes/" + strconv.Itoa(id)
	// req, err := http.NewRequest("GET", url, nil)
	// if err != nil {
	//  return "", err
	// }

	// resp, err := http.DefaultClient.Do(req)
	// if err != nil {
	//  return "", err
	// }

	// defer resp.Body.Close()
	// buf := new(bytes.Buffer)
	// buf.ReadFrom(resp.Body)
	// bytes := buf.Bytes()

	// joke := struct {
	//  Type  string `json:"type"`
	//  Value struct {
	//    Id   int    `json:"id"`
	//    Joke string `json:"joke"`
	//  } `json:"value"`
	// }{}
	// err = json.Unmarshal(bytes, &joke)
	// if err != nil {
	//  return "", err
	// }

	// return joke.Value.Joke, nil

}
